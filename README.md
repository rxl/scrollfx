ScrollFX.js
===========


Project Overview
----------------

Small, experimental, pure javascript module to make fancy scroll effects. 
 
**Version:** 0.2.0-alpha  
**Dependencies:** none (VanillaJS)  
**Support:** Latest Mozilla Firefox, Google Chrome and IE9+.    
Mobile browsers are also supported but performance is not so flawless.  
Blur effect works only on webkit based browsers (e.g. Google Chrome).

[SEE DEMO](http://rafaelpawlos.com/scrollfx)


Usage:
----------------

Here's an example of basic usage:

      scrollfx('.elements', {
        types: ['scale', 'blur']
      });

Options:
----------------

Option     | Values                                  | Description
---------- | --------------------------------------- | -----------
`types:`   | `['scale','cube','blur','opacity']`     | // Choose what effects apply to elements; default is scale
`scale:`   | `{steepness: 0.6, flatness: 1}`         | // **Steepness** - control intense of effect
`opacity:` | `{steepness: 1, flatness: 1.01}`        | // **Flatness** - control gap on screen center where elements are not transformed (increase readability)
`blur:`    | `{steepness: 1, flatness: 5}`           | 
`cube:`    | `{steepness: 0.30}`                     | 

You can also control appearance (e.g. cube perspective) in scrollfx.css. ScrollFX.js transforms are also animatable by css transition property. Feel free to experiment.

License
----------------

ScrollFX.js is released under the terms of the MIT license. See LICENSE file for details.


Credits
----------------

ScrollFX.js is developed by Rafael Pawlos, http://rafaelpawlos.com
