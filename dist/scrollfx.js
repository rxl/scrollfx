/*! scrollfx v0.2.0-alpha [27-04-2015] | (c) Rafael Pawlos (http://rafaelpawlos.com) | MIT license */

'use strict';

var scrollfx = (function () {

  var moduleObject = {
    init: function (opts, element) {
      var self = this;
      self.opts = opts;
      
      self.element = self.wrap(element);
      self.element.className = 'scrollfx-wrapper';
      
      self.supportedTransform = self.getSupportedTransform();

      if (opts.types.indexOf('scale') !== -1) {
        self.addListeners(window, 'load scroll resize', function () {
          self.updateScale();
        });
      }
      if (opts.types.indexOf('opacity') !== -1) {
        self.addListeners(window, 'load scroll resize', function () {
          self.updateOpacity();
        });
      }
      if (opts.types.indexOf('blur') !== -1) {
        self.addListeners(window, 'load scroll resize', function () {
          self.updateBlur();
        });
      }
      if (opts.types.indexOf('cube') !== -1) {
        var cube = self.assembleCube();
        self.addListeners(window, 'load scroll resize', function () {
          self.updateCubeTransform();
        });
      }
    },
    
    updateScale: function () {
      var self = this;
      self.changeElementProperty(self.element, self.supportedTransform, 'scale', '', 'parabola', 1, window.innerHeight, window.pageYOffset, self.element.offsetTop, self.element.offsetHeight, self.opts.scale.steepness, self.opts.scale.flatness);
    },
    
    updateOpacity: function () {
      var self = this;
      self.changeElementProperty(self.element, 'opacity', '', '', 'parabola', 1, window.innerHeight, window.pageYOffset, self.element.offsetTop, self.element.offsetHeight, self.opts.opacity.steepness, self.opts.opacity.flatness);
    },
    
    updateBlur: function () {
      var self = this;
      self.changeElementProperty(self.element, 'WebkitFilter', 'blur', 'px', 'parabola', -self.opts.blur.strength, window.innerHeight, window.pageYOffset, self.element.offsetTop, self.element.offsetHeight, self.opts.opacity.steepness, 0);
    },
    
    assembleCube: function () {
      var self = this;
      
      var top = document.createElement("div");
          top.className = 'scrollfx-plane-top';
          self.element.appendChild(top);

      var bottom = document.createElement("div");
          bottom.className = 'scrollfx-plane-bottom';
          self.element.appendChild(bottom);
    
      self.cube = {top: top, bottom: bottom};
    },
    
    updateCubeTransform: function () {
      var self = this;
      self.changeElementProperty(self.cube.top, self.supportedTransform, 'rotateX', 'rad', 'line', Math.PI / 2, window.innerHeight, window.pageYOffset, self.element.offsetTop, self.element.offsetHeight, self.opts.cube.steepness, Math.PI);
      var value = self.changeElementProperty(self.cube.bottom, self.supportedTransform, 'rotateX', 'rad', 'line', Math.PI / 2, window.innerHeight, window.pageYOffset, self.element.offsetTop, self.element.offsetHeight, self.opts.cube.steepness, 0);
       
      requestAnimationFrame(function () {
        if (value <= Math.PI / 1.8) {
          self.element.style.zIndex = Math.ceil(value * 100);
        } else if (value > Math.PI / 2) {
          self.element.style.zIndex = 0;
        }
      });
    },
    
    changeElementProperty: function (element, supportedProperty, propertyType, unit, functionShape, midValue, winHeight, scrollTop, elOffset, elHeight, steepness, flatness) {
      var self = this;
      if (scrollTop + winHeight > elOffset - 1.2*elHeight && scrollTop - 1.2*elHeight < elOffset) {
        switch (functionShape) {
          case 'parabola':
            var value = Math.max(Math.min(midValue * (-Math.pow((steepness * ((scrollTop + winHeight - elOffset) % (winHeight + elHeight)) + (Math.abs(1 - steepness) / 2) * (winHeight + elHeight)) / ((winHeight + elHeight) / 2) - 1, 2)) + flatness, Math.abs(midValue)), 0);
            break;
          case 'line':
            value = Math.max(midValue * ((steepness * ((scrollTop + winHeight - elOffset) % (winHeight + elHeight)) + (Math.abs(1 - steepness) / 2) * (winHeight + elHeight)) / ((winHeight + elHeight) / 2)) + flatness, 0);
            break;
        }
        
        self.currentFrame = requestAnimationFrame(function () {
          if (supportedProperty.toLowerCase().indexOf('transform') !== -1 || supportedProperty.toLowerCase().indexOf('filter') !== -1) {
            element.style[supportedProperty] = propertyType + '(' + value + unit + ')';
          } else {
            element.style[supportedProperty] = value + unit;
          }
        });

        return value;
      }
    },
    
    getSupportedProp: function (proparray) {
      var root = document.documentElement;
      for (var i = 0; i < proparray.length; i++) {
        if (proparray[i] in root.style) {
          return proparray[i];
        }
      }
    },
    
    getSupportedTransform: function () {
      var self = this;
      return self.getSupportedProp(['transform', 'MozTransform', 'WebkitTransform', 'msTransform', 'OTransform']);
    },
    
    addListeners: function (element, eventsString, callback) {
      var events = eventsString.split(' ');
      for (var i = 0, iLen = events.length; i < iLen; i++) {
        element.addEventListener(events[i], callback, false);
      }
    },
    
    wrap: function (toWrap, wrapper) {
      wrapper = wrapper || document.createElement('div');
      if (toWrap.nextSibling) {
        toWrap.parentNode.insertBefore(wrapper, toWrap.nextSibling);
      } else {
        toWrap.parentNode.appendChild(wrapper);
      }
      return wrapper.appendChild(toWrap).parentNode;
    }
  };
  
  function extend(a, b) {
    for (var key in b)
      if (b.hasOwnProperty(key))
        a[key] = b[key];
    return a;
  }

  return function (query, settings) {
    var elements = document.querySelectorAll(query);
    var opts = extend({
      types: ['scale'],
      scale: {
        steepness: 0.6,
        flatness: 1
      },
      opacity: {
        steepness: 1,
        flatness: 1.01
      },
      blur: {
        steepness: 1,
        strength: 5
      },
      cube: {
        steepness: 0.30
      }
    }, settings);

    Array.prototype.forEach.call(elements, function (el, i) {
      if (!el.scrollfx) {
        Object.create(moduleObject).init(opts, el);
        el.scrollfx = true;
      } else {
        console.warn('Detected double initialization of scrollfx module on %o', el);
      }
    });
  };
        
})();

// Create objects in older browsers
if (typeof Object.create !== 'function') {
  Object.create = function (obj) {
    function F() {
    }
    F.prototype = obj;
    return new F();
  };
}

// requestAnimationFrame polyfill
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());
